set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'bling/vim-airline'
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'
Plugin 'vim-scripts/camelcasemotion'
" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
" For vim-airline
set laststatus=2

" Window resize parameters. Increases size of current buffer to a defined standard; sets min size
set winheight=30
set winminwidth=5
set winwidth=100
set winminwidth=60

" Keybindings
"	Cycle window buffers is now Ctrl-<vim-direction>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"	Resize buffers with +/-, vertically with </>
nnoremap <silent> + :exe "resize " . (winheight(0) * 4/3)<Enter>
nnoremap <silent> _ :exe "resize " . (winheight(0) * 3/4)<Enter>
nnoremap <silent> > :exe "vertical resize " . (winwidth(0) * 4/3)<Enter>
nnoremap <silent> < :exe "vertical resize " . (winwidth(0) * 3/4)<Enter>
"	Remap Fixit YCM command to easy use
map <C-F> :YcmCompleter FixIt<Enter>
map <C-Y> :YcmDiag<Enter>



" Set dir tree style
let g:netrw_liststyle=3

syntax enable
colorscheme monokai

" swap is annoying
set noswapfile

set exrc  " Allows for project specific .vimrc files in local project folders
set secure " Does not allow crazy .vimrc writes in nondefault places

set number
highlight CursorLine cterm=NONE ctermbg=NONE ctermfg=NONE guibg=NONE guifg=NONE
set cursorline
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

set colorcolumn=110
highlight ColorColumn ctermbg=darkgray

" gf command can open files under cursor. Put something like the following to specify location to look for said files
let &path.="/home/jcastagneri/dev/caffe/include/caffe/,/home/jcastagneri/dev/caffe/src,"

